# ssh-keys

## Repo para descarga de llaves SSH publicas de JavaNS, Proveedores, Clientes, etc

- Proveedores: crear una sub-carpeta con el nombre del proveedor en minusculas y luego cargar las llaves correspondientes, respetando la nomenclatura, nombreProveedor_InicialNombreApellido.pub
- Clientes: crear una sub-carpeta con el nombre del proveedor en minusculas y luego cargar las llaves correspondientes, respetando la nomenclatura, nombreCliente_InicialNombreApellido.pub o nombreCliente_InicialNombreInicialApellidos.pub